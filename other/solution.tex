\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{algorithmic}
\usepackage[english,ruled]{algorithm2e}
\usepackage[T1]{fontenc}
\usepackage{lmodern}

\title{Edge membership}
\author{Francisco Magalhães }
\date{July 2018}

\begin{document}

\maketitle

\section{Introduction}
In this document we will describe the main topics of our solution for a scalable membership for edge systems. Firstly, we used Jason Brown’s approach for implementing changes in the current Cassandra gossip subsystem, mainly when it comes to the membership. Cassandra's membership is one-hop, allowing each node to communicate directly with any other. However, this introduces some problems in terms of scalability, due to the cost of maintaing so many connections at the same time. Brown proposed  and implemented a peer sampling service to create partial views as well as a broadcast tree, based on both HyParView and Thicket respectively. Those changes were integrated directly in cassandra’s source code, which we used as a baseline. We then carefully extracted all the code regarding the membership and inerent code, to make it usable by other systems, through the use of a well defined set of functions. We then added many more features concerning dissemination throughout the trees, as well as an edge service.

\section{Architecture and implementation}

\subsection{Peer Sampling Service}
Firstly, we will describe the part of the solution regarding the communication within multiple data centers. We propose the use of a Peer Sampling Service which provides a partial view of a cluster to each node, allowing much more efficient resource management and dissemination of information. This way, there is no need to maintain a connection between every pair of node over the cluster. A combination of all the partial views creates a fully-connected mesh.

HyParView is a clearly defined protocol for a Peer Sampling Service. It is used for building unstructured overlays and balances both in and out-degrees of nodes in the overlay. It is a highly scalable gossip based membership protocol. It is able to offer high resilience and high delivery reliability for message broadcast using gossip on top of the overlay even in churn scenarios. It relies on a hybrid approach (using cyclic and reactive strategies), by maintaining a passive view and a symmetric active view in each node. Furthermore, HyParView ensures complete coverage by making sure all nodes are included in at least one node's view, through the use of a coordination protocol.

When a node first joins the system, it will firstly contact a seed node, which will then proceed to send ForwardJoin messages as stated in HyParView’s paper, and so on. The address of the seed node must come from an external source.

\subsubsection{Backup view}
Despite being described as having those two views, the used base implementation skips the passive view, eliminating mainly some extra network traffic. However, the functionality of the view is still retained, as we can piggy-back off the existing knowledge of the full cluster (which is then broadcasted through the use of trees, as we will describe next) in the heartbeat messages used for failure detection. Information (i.e. the state) on those nodes in the backlog is maintained, although they are only used for communication when necessary, as described in the paper. 

\begin{algorithm}[t!]
\caption{Data structures\label{alg:agg}}
{\footnotesize
\begin{tabbing}
xxxxx\=xx\=xx\=xx\=xx\=xx\=xx\=xx\kill
1: \textbf{Local State:}\\
2: \> $N_{id}$ \texttt{//Node address}\\
3: \> $N_{datacenter}$ \texttt{//Node data center}\\
4: \> localView \texttt{//List: Address}\\
5: \> remoteView \texttt{//Map: $remoteView[ N_{datacenter} ] \rightarrow$ Address}\\
~\\
6: \textbf{procedure} getPeers \textbf{do}\\
7: \> remote $ \leftarrow \emptyset $ \\
8: \> \textbf{foreach} $N_{datacenter} \in remoteView$ \textbf{do}\\
9: \>  \> remote $\cup $ \{$remoteView[ N_{datacenter} ]$\} \\
10: \> \textbf{return} localView $\cup $ $remote$ \\
\end{tabbing}
}
\end{algorithm}

\subsubsection{Multiple data centers}
In addition, the implementation takes into account the possibility of multiple data centers belonging to the same system. This means that nodes from different data centers take part in the same unstructured overlay created using HyParView. In order to do that, a remote active view is maintained (Algorithm 1, line 5), containing information on nodes from other data centers. The remote view will hold at maximum one active peer for each remote data center. The backlog containing all the nodes in the system allows each node to acquire knowledge on the number of nodes in each data center. This information is used so we can guarantee that a HyParView NeighborRequest message for another data center is only sent if the number of local nodes (i.e. nodes from the local data center) is less than or equal to the number of nodes in the remote data center (Algorithm 2, line 10). This approach ensures that nodes in the smaller data center are the ones trying to connect. Otherwise, we could be sending too many messages to those smaller data centers. Let \textit{dc1} be a data center with 10 nodes and \textit{dc2} a bigger data center with 20 nodes. At a given time, the aggregate remote view (i.e. the combined remote view of each node) in \textit{dc2} may contain all the 10 nodes in \textit{dc1}. However, the reverse will not be true, as there will be at most 10 nodes from \textit{dc2} (instead of 20) in the aggregate remote view of \textit{dc1}. HyParView does not deal with network partitions, so this approach is used to guarantee there is always a way between all data centers and that events in \textit{dc1} will be propagated throughout \textit{dc2} and vice-versa. With this modifications, the information sent during membership operations in the HyParView is always sent along with the data center identifier.




Algorithm 2 depicts the most important divergences when maintaining both views when compared to the original algorithm. The procedures that handles disconnect messages is a bit different using this approach. Whenever a node receives a disconnect message, it will now try to replace the removed node with another one from the same data center (Algorithm 2, line 14), unless the necessary conditions are not verified. This way we are trying to keep the connectivity between data centers as much as possible. Furthermore, in order to have at most one node from each data center in the remote view, a high priority NeighborRequest from another data center will be accepted and the local node may have to disconnect from an existing peer from that same data center (Algorithm 2, line 37).

\begin{algorithm}[t!]
\caption{Neighbor handlers\label{alg:agg}}
{\footnotesize
\begin{tabbing}
xxxxx\=xx\=xx\=xx\=xx\=xx\=xx\=xx\kill
1: \textbf{upon} Receive(Disconnect, peer, datacenter) \textbf{do}\\
2: \> existsLocal $\leftarrow $ peer $ \in $ localView  \\
3: \> existsRemote $\leftarrow $ peer $ \in remoteView[$N_{datacenter}] $   \\
4: \> \textbf{if} $ existsLocal $ \lor $ existsRemote \textbf{then}  \\
5: \> \> localView $\leftarrow $ localView $\setminus $ \{peer\}  \\
6: \> \> remoteView[$N_{datacenter}] $ \leftarrow $ remoteView[$N_{datacenter}] $ \setminus $ \{peer\}  \\
7: \> \> sendRequest $\leftarrow $ false \\
8: \> \> \textbf{if} existsLocal \textbf{then}  \\
9: \> \> \> sendRequest $\leftarrow $ isFull(localView) \\
10: \> \> \textbf{if} existsRemote \textbf{then}  \\
11: \> \> \> sendRequest $\leftarrow $ size($N_{datacenter}$) $ \leq $ size(datacenter) \\
12: \> \> \textbf{if} sendRequest \textbf{then} \\
13: \> \> \> \textbf{trigger} sendNeighborRequest(datacenter) \\
~\\
14: \textbf{procedure} sendNeighborRequest(datacenter) \textbf{do} \\
15: \> newPeer $\leftarrow $ getRandomPeer(datacenter)  \\
16: \> Send(NeighborRequest, newPeer, $N_{id}$, $N_{datacenter}$) \\
~\\
17: \textbf{upon} Receive(NeighborRequest, priority, peer, datacenter) \textbf{do}\\
18: \> existsLocal $\leftarrow $ peer $ \in $ localView  \\
19: \> existsRemote $\leftarrow $ peer $ \in remoteView[$N_{datacenter}] $   \\
20: \> \textbf{if} $ existsLocal $ \lor $ existsRemote \textbf{then}  \\
21: \> \> Send(NeighborResponse, ACCEPT, $N_{id}$) \\
22: \> \> \textbf{return} \\
23: \> \textbf{if} $ priority $ == $ LOW $ \textbf{then} \\
24: \> \> \textbf{if} $ (datacenter $ == $ N_{datacenter} $ \land $ isFull(localView)) \\
\> \> \lor $ remoteView[$N_{datacenter}] $ \neq $ \emptyset $ $ \textbf{then} $\\
25: \> \> \> Send(NeighborResponse, DENY, $N_{id}$) \\
26: \> \> \> \textbf{return} \\
27: \> \> \textbf{trigger} addNodeToActiveView(peer) \\
28: \> \> Send(NeighborResponse, ACCEPT, $N_{id}$) \\
~\\
29: \textbf{procedure} addNodeToActiveView(peer, datacenter) \textbf{do} \\
30: \> \textbf{if} $ datacenter $ == $ N_{datacenter} $ \textbf{then} \\
31: \> \> localView $ \leftarrow $ localView $\cup $ \{peer\} \\
32: \> \> \textbf{if} $ isFull(localView) $ \textbf{then} \\
33: \> \> \> Send(Disconnect, first, $N_{id}$, $N_{datacenter}$) \\
34: \> \> \textbf{return} \\
35: \> remoteView[$N_{datacenter}] $ \leftarrow $ remoteView[$N_{datacenter}] $ \cup $ \{peer\}  \\
36: \> \textbf{if} $ size(remoteView[$N_{datacenter}]) > 1 $ \textbf{then}\\
37: \> \> Send(Disconnect, first, $N_{id}$, $N_{datacenter}$) \\
\end{tabbing}
}
\end{algorithm}


\subsection{Broadcast Tree}
We will now describe the used approach for disseminating information. The used solution relies on Thicket, which describes dynamic and self-healing broadcast trees. Thicket efficiently disseminates information in a peer-to-peer overlay by making use of an underlying peer sampling service such as HyParView, to build the overlay network. The algorithm computes a tree for each node in the cluster (in which the root is each of the nodes). The broadcast tree allows a node to efficiently send updates to all the other nodes in the cluster with the use of a balanced, self-healing tree based on the node partial view.  Each node maintains a set of active peers, derived from the PSS partial view. Whenever a message has to be broadcasted from a node, it will be sent to its active peers. Each node also keeps a set of backup nodes which can later become part of the active peers as described in the paper.  In addition, it also maintains a set of active peers for each peer. Whenever a message is received with the root being a given node, the respective active peers set will be used as destinations for next step of dissemination. The implementation follows the paper closely. Each node in the whole system is the root of a tree. Both remote and local HPV views are used to derive the set of peers in each tree, so a tree should contain all nodes from all data centers. Each tree is used to disseminate messages originated at its root to all the other nodes.

With the introduction of an edge service, a little change has to be made when receiving data from a peer. This modification consists in broadcasting the same data to the necessary levels of the edge hierarchy (Algorithm 3, line 2 - referring to Algorithm 5, line 27). Details on this topic will be explained in the next section. The parameters used in the header of the Receive function are the same ones as in the Thicket paper.

\begin{algorithm}[t!]
\caption{Thicket changes\label{alg:agg}}
{\footnotesize
\begin{tabbing}
xxxxx\=xx\=xx\=xx\=xx\=xx\=xx\=xx\kill
1: \textbf{upon} Receive(DATA, m, muid, load, tree, sender) \textbf{do}\\
2: \> Edge.broadcast(m) \\
3: \> etc. \\
\end{tabbing}
}
\end{algorithm}

\subsubsection{Local broadcast}
Additionally, our implementation uses a secondary instance of Thicket implementation in order to build and maintain local trees, i.e. trees that contain only nodes from each data center. This is achieved by using only HPV’s local active view, instead of both local and remote (as explained before, the partial view we obtain from the PSS is a main factor for the nodes that will be chosen in each tree). This way, no tree will contain nodes from more than one data center. For this reason, we are able to propagate any message exclusively through the data center from which the message was originated. Some systems can possibly make use of a primitive which allows local-only broadcast. This result could also be obtained by having two instances of HyParView running in each node (one with only local nodes and the other with both local and remote nodes). However, we find it is more convenient to use our approach both in terms of implementation and performance. Each Thicket message contains a flag saying whether we are aiming for a local broadcast or not and it will chose the right Thicket instance to handle the request according to the value of that flag.

\subsection{Edge Service}
We will now describe the architecture of the whole membership when allowing edge devices as part of it, and later we will describe the details of our implementation. A private network connected to a data center may contain many devices which can be used as access points for the clients in that network. A personal laptop can then be used as node of the system, taking part in the membership and keeping track of some other nodes in the data centers. In the same way, a second private network inside the first one may have some devices which can also be seen as data center nodes, from a client’s point of view. This networks will obviously contain only a small part of the information existing in the “main” nodes. The goal is to allow clients to faster update and receive information which is more likely accessed by them. For this reason, we build a hierarchy of networks, in which the network containing the data centers is at the top, and the most restrictive network is at the bottom.

\begin{algorithm}[t!]
\caption{Data structures\label{alg:agg}}
{\footnotesize
\begin{tabbing}
xxxxx\=xx\=xx\=xx\=xx\=xx\=xx\=xx\kill
1: \textbf{Local State:}\\
2: \> $N_{id}$ \texttt{//Node address}\\
3: \> $N_{datacenter}$ \texttt{//Node data center}\\
4: \> upView \texttt{//List: Address}\\
5: \> downView \texttt{//List: Address}\\
6: \> filter \textit{//BloomFilter } \\
7: \> filters \texttt{//Map: $Address \rightarrow$ BloomFilter }\\
\end{tabbing}
}
\end{algorithm}

The same way a node joining the system needs the address of a seed node to initialize the HyParView join procedure, an edge node needs a contact node from a network right above in the hierarchy (i.e. its parent network), also provided by an external source.

An edge node also runs HyParView and Thicket as a common data center node. In consequence, each network has an overlay network built on top of it, where nodes communicate through it.

Each node maintains a bloom filter and a map of received ones. Each bloom filter is used to keep track of the information to which each node is subscribed. A node being subscribed to a keyword means it will replicate information mapped to that keyword. At first, an edge node inserts in its bloom filter the keywords to which it will be subscribed, as in a publisher/subscriber architecture. An example with a test application will be explained later for better understanding. Bloom filter for this purpose due to the small amount of information it requires to keep. Without having to exchange the keywords directly, we still achieve the same goal.

\subsubsection{Join Mechanism}
When an edge node tries to join the system, in addition to starting the HyParView join procedure, it will first load its bloom filter and then send an EdgeJoin message to the specified contact node from the above level in the hierarchy, as described before. The contact node will then answer with an EdgeJoinResponse containing the map of bloom filters it has received until the moment. The local node will then use the broadcast protocol (Thicket, in this case) to disseminate that same response through its network, so the other nodes can update the filters information as well. This allows every node to keep track of the location of each data. This whole procedure is repeated periodically, to make sure every node has updated information as well as to ensure that nodes that just joined the membership have access to that as well. Moreover, any node in the hierarchy is subscribed only to a subset of the keywords to which the nodes in the level right above it are subscribed. Otherwise, the membership will not behave as expected. The top level contains all the information existing in the system.

Each edge network can have only one connection to the superior level. On the other hand, two different edge networks in the same level can maintain an edge connection two the same data center, and even to the same node.

\begin{algorithm}[t!]
\caption{Edge operations\label{alg:agg}}
{\footnotesize
\begin{tabbing}
xxxxx\=xx\=xx\=xx\=xx\=xx\=xx\=xx\kill
1: \textbf{upon init do}\\
2: \> fillFilter(filter, keywords) \\
3: \> Send(Join, contactNode, filter, $N_{id}$, $N_{datacenter}$) \\
~\\
4: \textbf{upon} $ Receive(Join, filter, newNode, datacenter) $ \textbf{do} \\
5: \> filters $ \leftarrow $ filters $ \cup $ \{(newNode, filter)\} \\
6: \> Send(JoinResponse, newNode, filters, $N_{id}$, $N_{datacenter}$) \\
~\\
7: \textbf{upon} $ Receive(JoinResponse, remoteFilters, node, datacenter) $ \textbf{do} \\
8: \> Thicket.broadcast(JoinResponse, remoteFilters, node, $N_{id}$) \\
9: \> filters $ \leftarrow $ filters $ \cup $ remoteFilters \\
~\\
10: \textbf{upon} $ Receive(Data, data, root, node, datacenter) $ \textbf{do} \\
11: \> Thicket.broadcast(Data, data, $N_{id}$) \\
12: \> \textbf{foreach} $ address \in $ upView \textbf{do}\\
13: \> \> \textbf{if} $ address \neq $ root $ \land $ address $ \neq $ node \textbf{then}\\
14: \> \> \> Send(Data, data, address, root, $N_{id}$, $N_{datacenter}$) \\
15: \> \textbf{if} $ interested(data) $ \textbf{then} \\
16: \> \> \textbf{foreach} $ address \in $ downView \textbf{do}\\
17: \> \> \> \textbf{if} $ address \neq $ root $ \land $ address $ \neq $ node $ \land $ interested(data, address) \textbf{then}\\
18: \> \> \> \> Send(Data, data, address, root, $N_{id}$, $N_{datacenter}$) \\
~\\
19: \textbf{procedure} $ interested(data, address) $ \textbf{do} \\
20: \> keywords $ \leftarrow $ getKeywords(data) \\
21: \> \textbf{foreach} $ word \in $ keywords \textbf{do} \\
22: \> \> \textbf{if} $ filters[$address].contains(word) \textbf{then} \\
23: \> \> \> \textbf{return} true \\
24: \> \textbf{return} false \\
~\\
25: \textbf{procedure} $ interested(data) $ \textbf{do} \\
26: \> \textbf{return} filter.contains(word) \\
~\\
27: \textbf{procedure} $ broadcast(data) $ \textbf{do} \\
28: \> \textbf{foreach} $ address \in $ upView \textbf{do}\\
29: \> \> Send(Data, data, address, $N_{id}$, $N_{datacenter}$) \\
30: \> \textbf{if} $ interested(data) $ \textbf{then} \\
31: \> \> \textbf{foreach} $ address \in $ downView \textbf{do}\\
32: \> \> \> \textbf{if} $ interested(data, address) $ \textbf{then}\\
33: \> \> \> \> Send(Data, data, address, $N_{id}$, $N_{datacenter}$) \\
\end{tabbing}
}
\end{algorithm}

\subsubsection{Dissemination}
Any payload needed to be sent by the application is encapsulated in an EdgeData message. Every EdgeData message is propagated to the upper levels of the network hierarchy (Algorithm 4, line 12), although not always propagated to the levels bellow (Algorithm 4, line 17). For this reason, each node has to maintain two data structures. One for contact nodes (which will be only one at the time, but we'll keep it as a collection for further improvements) and another one for nodes in a level bellow to which it maintains an edge connection. Having access to each level’s bloom filter, any node is able to understand if lower levels are interested in receiving some payload. There is no need in sending those levels a message they are not interested in. For instance, the dissemination of a message coming from a node in the data center can stop at some level in the hierarchy or not even reaching any level at all (excluding the data center). Additionally, if a receiving node understands that it is interested in a given payload received inside an EdgeData message, the payload is extracted and encapsulated in a ThicketData message and it will then be disseminated through the respective tree. In the same way, any node that received a ThicketData message will extract the payload and encapsulate it in an EdgeData message and send it through its edge connections (if the other ends are interested), if any.

\section{API}
Our Java API provides a well defined set of functions that can be used by applications needing a membership service which allows edge connections.

\begin{itemize}
    \item \textbf{initServer()} Allows each service (HyParView, Thicket and Edge Service) to be initialized
    \item \textbf{shutdown()} Shuts down any service running in the local node
    \item \textbf{getMembers()} Returns an iterator for members of the membership of which the local node is aware of
    \item \textbf{getPeers()} Returns an iterator for all nodes in both the local and remote active views
    \item \textbf{getLocalPeers()} Returns an iterator for the nodes in the local active view, i.e. only nodes from the local data center
    \item \textbf{broadcast(IPayload payload)} Broadcasts a given payload both through the tree and edge connections, in order to reach every end of the membership
     \item \textbf{datacenterBroadcast(IPayload payload)} Broadcasts a given payload only through the local datacenter
    \item \textbf{interested(IPayload payload)} Returns true if the local node is interested in a given payload, considering its keywords. Otherwise, returns false
    \item \textbf{where(Object keyword)} Returns one address which is interested in the given keyword
    \item \textbf{addListener(BroadcastServiceClient listener)} Adds a listener to Thicket service as well as to Edge service. A listener must implement the method \textit{BroadcastServiceClient.receive(IPayload payload)} to specify a behaviour upon receiving a message either through the tree or through an edge connection
\end{itemize}

\textit{IPayload} defines an interface with only one method: \textit{getKeywords()}. Any ad-hoc payload object must implement \textit{IPayload} and in consequence implement \textit{getKeywords()}, to be used by the Edge service to check whether a node is interested in the data.

\section{Client-Server example}

\subsection{Architecture}
In order to test the functionalities provided by our API, we implemented a simplified version of Twitter.

Each server node is part of the membership and also handles client connections. Additionally, it is subscribed to a given set of keywords. As we explained before, a node has to be subscribed only to a subset of keywords from the keywords of the nodes in its parent level. The keywords used in this example will be hashtags (e.g. \textit{\#twitter}, \textit{\#friends}, etc.). A data center node will be subscribed (and therefore interested) to all the used hashtags. An edge network may then be subscribed to the hashtags \textit{\#twitter, \#social, \#friends, \#network}, and an edge network inside the first one may be subscribed to \textit{\#twitter, \#social}. The set of keywords to which a node is subscribed is provided beforehand in each node's configuration file. Therefore, it will keep track of every tweet containing those hashtags, maintaining a structure that maps hashtags into lists of tweets.

\subsection{API usage}
Before calling \textit{initServer()} in a server node, a listener has to be added using \textit{addListener()}. The listener class to which we called \textit{GossipTweetListener<Tweet>} implements \textit{BroadcastServiceClient<IPayload>}. It will then implement \textit{receive (Tweet t)}. In our application's case, upon receiving a tweet in which it is interested (\textit{interested(tweet)}), the node will just update its map of hashtags and tweets and then send the tweet \textit{t} to the clients with whom it has an opened connection. The dissemination through the membership is handled elsewhere and shall not be the concern of the application.

Each client communicates with only one server at a time. A client has two possible operations: tweet and search.
\begin{itemize}
    \item \textbf{tweet(Tweet t):} sends \textit{t} to server.
    \item \textbf{search(String hashtag):} asks server for all the tweets containing \textit{hashtag}.
\end{itemize}

Whenever a server receives a tweet from the client (class \textit{Tweet} implements \textit{IPayload}), it uses the API function \textit{broadcast()}. The tweet will then reach every other server node which is either interested in the payload, in the same overlay as that server, or higher in the hierarchy. Consecutively, if a node received the disseminated tweet and it is interested in it, the tweet will reach any client connected to that same node. Otherwise it will just relay the message.

Whenever a server receives a search request, it will first try to understand whether it is subscribed to \textit{hashtag}. If so, it will just send to the client all the tweets it stored with that hashtag. Otherwise, it will use the result of the API function \textit{where()} to tell the client the address of a server which is subscribed to that keyword. Upon receiving the address, the client's behaviour consists solely in closing the current connection, opening connection to the referred server and automatically repeating the operation.

\subsection{Discussion}
Clients in an edge network are expected to request data contained in the server to which they are connected. As we described, if that is not the case and a client searches for something else (even for only once), a new connection has to be made. This scenery can repeat itself, as we keep searching for other information. As we can see given this implementation, the system will eventually converge to a state where every client is connected to a node in the data center. Although this is a problem that concerns the application itself and not the membership service we described, we can still propose three main solutions for this, which can even be used together:
\begin{itemize}
    \item Keeping track of each client's request pattern. As we explained, a client that was once connected to a lower level of the network hierarchy may have requested some information that made him connect to a higher level. If that client goes back to the previous request pattern where he requests information contained in the lower level, we can close the current connection and open another one with a node from that level.
    \item Keeping track of the general request pattern. If a server receives \textit{n} requests containing the keyword \textit{k}, it can subscribe to that keyword and start replicating the corresponding information. However, this implies that the higher levels also subscribe that keyword. This will eventually converge to a scenery where every node is interested in every payload, increasing network traffic and node overload. Nonetheless, this can be mitigated using a maximum number of keywords per node.
    \item Not closing connections. When needing to connect to a higher level node, the client simply keeps the current connection opened. The client can then use one of the opened connections when requesting. Although this allows to keep fast access to the server when possible, we still don't mitigate the convergence problem. 
\end{itemize}
\end{document}
