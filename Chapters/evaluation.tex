%!TEX root = ../template.tex
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% chapter2.tex
%% NOVA thesis document file
%%
%% Chapter with the template manual
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Evaluation}
\label{cha:evaluation}

In this chapter we present and analyze the results of our experimental work, comparing our final solution with others. We also have deployed a client-server application that makes use of our API to meet its requirements. We describe and evaluate the application and also suggest some improvements to similar applications when using our API.

\section{Experimental work}
In this section we start by describing our setup, which we used for all the tests with our membership service.  We then present our results and analyze them.

We compare three approaches in this chapter: our solution (MS-DCS), our solution but with no data center distinction (MS-DC1), and Cassandra's membership (CM). MS-DC1 solution simply removes the use of remote views from the solution and handles nodes as if there are no different data centers (multiple data centers are treated as one). In order to make a fair comparison between the different solutions, we do not use any edge nodes in the first set of experiences. After the comparison between the three solutions, we then evaluate some properties of our membership when including edge nodes, as it is one of the main goals of our work.

\subsection{Experimental setup}
We have run our experiments in Microsoft Azure, a well-known platform which provides cloud computing services. As our membership service aims to take into account the use of different data centers, we have used 4 different data centers, located in Netherlands, the United States, France and Canada. In each data center, we have created a virtual network.

In order to simulate a scenario of a large-scale system, we wanted each data center to have a number of nodes relatively greater than the expected number of peers in a node's partial view, in order to be able to test the peer sampling service with great reliability. However, we had to limit to number of nodes per data center to 10 due to budget limitations on using Microsoft Azure. With this number of nodes, each node is expected to have less than 4 peers in its local active view, which represents the fanout value. We then used 10 \textit{A2 v2} virtual machines in each data center to emulate each of the 10 nodes, by running an instance of our membership in each one (with the same source code). An \textit{A2 v2} type virtual machine has two CPU cores as well as 4 gigabytes of RAM, which we find suitable for our experiences.

Each node was provided with the public address of four seeds, one from each data center. The seed list is the same in every node. Each node also knows its own public address beforehand. Port 7000 (the default port for our membership) had to be previously open, for TCP connections, for membership nodes to be able to receive packets from the other virtual networks.

For the following tests, unless stated otherwise, we started the membership service at the four seed nodes. After thirty seconds, we started the service in the remaining 36 nodes. However, since we access the virtual machines through \textit{ssh}, the starting times can vary as it depends on the latency between the local experience orchestration machine and the data centers. Furthermore, we have disabled the shutdown notification dissemination mechanism so we could simulate real failures. Otherwise closing a node would simply notify the membership through its tree.

\subsection{Multi-data center}
\subsubsection{Open connections}
We first present our results for the average number of open TCP connections throughout time of in experience. We have separated this information in Fig. \ref{fig:seedconnections} and Fig. \ref{fig:commonconnections} for both seed nodes and non-seed nodes respectively.

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{seedconnections.png}
\caption{Open TCP connections through time in seed nodes} \label{fig:seedconnections}
\end{figure}

As for seed nodes, we can observe that after we have started the other 36 nodes, the number of open connections starts to increase quickly. Due to our mechanism of preventing excessive gossiping to seed nodes, by checking if a node is already gossiping to that data center through its partial view, we are able to reduce the number of open connections when comparing to Cassandra. In Cassandra's membership, as expected, the number of open connections is constant over time after it reaches the total number of nodes (minus one, itself) for the first time. In this case, that value is 39. Both MS-DCS and MS-DC1 solutions present roughly the same number of open connections over time. One minute and fifteen seconds (75 seconds) after we have initialized the other nodes, we can see that the number of open connections starts to drop. Eventually, the number of open connections reaches the number of peers in a node's partial view, which is smaller in MS-DC1 since \textit{log(40)+1 < log(10)+1+3}. The value \textit{log(N)+1} represents the average number of nodes in local partial views, as explained in the previous chapter. Since in MS-DC1 there is no distinction between data centers, then \textit{N=40}, while in MS-DCS \textit{N=10}. The constant 3 refers to the number of peers in the remote active view, which corresponds to one per each remote data center, which is only applied to MS-DCS.

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{commonconnections.png}
\caption{Open TCP connections through time in non-seed nodes} \label{fig:commonconnections}
\end{figure}

In non-seed nodes, i.e. the remaining 36, the behavior is not significantly different. However, in both MS-DCS and MS-DC1, nodes reach a smaller number of open TCP connections than in seed nodes. That number is still higher than the number of peers in partial views due to the constant replacement of nodes in those views in scenarios with lots of nodes joining the overlay at the same time, as it is the case. We can also verify that the number of connections may even increase and decrease again. This is due to the periodic mechanism for ensuring node's full cluster connectivity: when a node considers its partial view is too small considering the total number of nodes, it sends a Neighbor request to a node from its passive/backup view.

Although latency is not influenced by this number of open connections, we are preventing eventual network saturation in large-scale scenarios, with data centers with thousands of nodes.

\subsubsection{Messages sent}
In this experiment we have measured the number of messages sent (to maintain the membership and dissemination components) by each solution over time. We are also able to see how this number changes when a high number of nodes join the membership. This measurement is highly relevant since too many messages may slow down other important processes in the machine.

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{seedmsg.png}
\caption{Messages sent per second in seed nodes} \label{fig:seedmsg}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{commonmsg.png}
\caption{Messages sent per second in non-seed nodes} \label{fig:commonmsg}
\end{figure}

As the results show in Fig. \ref{fig:seedmsg}, CM introduces more messages sent per node right after start up. This is due to the echo messages (both the requests and the confirmations) sent to confirm nodes' reachability after learning about them. When the other 36 nodes are joined, the number of messages sent per second increases quickly regardless of the used approach. While in the CM solution, this is due to the echo messages, in MS-DCS and MS-DC1 solutions, this is due to the state propagation mechanism that is triggered in each of the 36 nodes, which originates more sent messages. Roughly the same number of messages is distributed in both MS solutions through seconds 30 to 35.

Fig. \ref{fig:commonmsg} shows that non-seed nodes send a lot of messages in CM a couple of seconds after joining them, due to the echo messages. Since a node takes longer to be aware of the whole cluster in CM than in both MS solutions, those messages will only be sent 3 or 4 seconds after the nodes were joined (as we will show and explain in detail in the test in section \ref{sec:jointest}). In both MS-DCS and MS-DC1, the messages concerning nodes' state propagation through periodic gossip exchanges are sent earlier and are a lot less than in CM. This is due to the fact that through HyParView's joining procedures, nodes will be aware of others much faster.

There is no significant difference in terms of the number of messages sent between MS-DCS and MS-DC1. On the other hand, we can observe that a periodic increase of that number. This is due to tree construction, which requires SUMMARY and GRAFT messages to be sent periodically. Although they are not needed for the construction of the global view, that procedure accelerates eventual data dissemination, as detailed in \ref{sec:thicketjoin}. After every tree is stable, all three solutions present roughly the same number of messages per second. CM presents slightly more messages, since nodes still gossip to seeds if they have not gossiped to one on that same second.

\subsubsection{Join}
\label{sec:jointest}
As one of our goals is to quickly make nodes aware of changes in the membership, we joined a node in a stable environment. We firstly started 39 nodes and after a couple of minutes we have started the 40th node. The 40th node was the same in the tests for the three approaches. One goal of this test is to measure how long it takes for a node to be aware of the whole membership (knowing the state of every other node). The other goal is to measure how long it takes for every other node to learn about the new node.

\begin{table}[]
\centering
\begin{tabular}{llll}
\hline
& MS-DCS & MS-DC & CM \\
Global cluster information & 1.8    & 1.8   & 4.7 \\
New node information (average) & 0.60   & 0.80  & 5.3 \\
New node information (maximum) & 0.67   & 0.9   & 8.7 \\ 
\hline
\end{tabular}
\caption{Times (s) for global view construction when joining node}
\label{table:jointimes}
\end{table}

The first row of Table \ref{table:jointimes} refers to the time it took (in seconds) for a node to learn the state of the whole cluster. The second row is the average time it took for other nodes to learn about the state of the new one. Finally, the third row represents the time it took to the last node receiving the update to learn about the state of the new node.

In both MS solutions, the new joining node is able to learn about the whole cluster more than two times faster than the CM solution. As soon as the node starts, it tries to reach a seed. Quickly after, it will have new nodes in its active view, which is symmetric. For that reason, at least one of its new peers will likely (since the choice is random between the peers of the active view) gossip the cluster state on their next periodic gossip exchange, set to trigger every second. On the other hand, in Cassandra's solution, the new node has to first gossip to a seed, which will mark it as alive. In the next of seed's gossip cycle, it will gossip to a random live node, which means it can either gossip the cluster state to the new node or gossip with another node, already including the state of the new one. The joining node then has to wait one of the nodes to initiate a gossip exchange with it. The relation between the time it takes and the number of nodes in the cluster is then logarithmic and also influenced by the gossip cycle timer.

We can also verify a big difference between the MS's and the CM solutions while measuring how long it takes for each node to be aware of the joining node. As we are making use of our broadcast service built on top of Thicket, the dissemination of the update is much faster than Cassandra's periodic gossip approach. There is also a slight difference between the times of MS-DCS and MS-DC1 solutions. This is due to the fact that MS-DC1 does not use any criteria relative to the physical distancy (i.e. the data centers) for choosing peers for the active view. In consequence, a node might propagate the update to too many remote peers and too few or none local peers. Therefore, the update message might travel many times between data centers, taking longer to reach every node.


\subsubsection{Failure detection and network partitions}
For this test, we have simulated a cross-continent network partition between Europe and America, separating the data centers in Netherlands and France from the ones in Canada and the United States. This simulation was done through an adaptation of the source code, by making each node ignore messages received from the data centers from which it will be separated, after a given time. Since connections are closed after a while if they are not being used, the TCP sockets between the unreachable data centers will be closed.

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{partition.png}
\caption{Number of live nodes from the point of view of one node} \label{fig:partition}
\end{figure}

We first started the 40 nodes. After 150 seconds, we introduced the network partition between continents. Fig. \ref{fig:partition} represents the average number of live nodes through time from the point of view of a single node. The dots represent changes in the number. We can note after we have introduced the partition, it took from 20 to 25 seconds until every node from the other continent was considered as unreachable and therefore dead. The average time of failure detection depends on how often a node is reported as live which in turn depends on the timer for gossip exchanges.

%If the timer was set to more than one second, failures would take longer to be detected, since the 

After 100 more seconds, we have removed the network partition. Each node took approximately one second to completely recover from it, i.e. too mark the nodes from the other continent as alive. Since there is an attempt to gossip with an unreachable node every second, nodes will exchange the states they know with only one gossip exchange.

These results do not depend on which of the three solution is used, since the same mechanism is implemented without any differences in all of them.

\paragraph{}Additionally, even though it is not likely that every peer in a node's partial view fails at more or less the same time, such scenario may happen. In that case, any message originated by that node before it detects the failures will be completely lost. Since the recovery summary mechanism in Thicket makes use of a backup view that is derived from the partial view, the node will try to send both the message and later the summary to members of its partial view, which are inactive. One solution for this issue would be retrying to send the message when connected to another node, although it could require keeping the data during longer periods of time.

\subsection{Edge Scenario}

\subsubsection{Experimental setup}
\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{experiment.png}
\caption{Initial setup for edge tests} \label{fig:experiment}
\end{figure}

For the tests concerning the edge functionality, we used the same data center configuration as in the previous tests. We also had two main edge networks protected by firewalls and NATs. The network in \textit{dce1} has only one node. The network in \textit{dce2} has four nodes, in which two of them were simulated to be inside a network contained in \textit{dce2}. The node in \textit{dce1} uses a node from \textit{dc1} (Netherlands), while \textit{dce2} uses a node from \textit{dc4} (Canada) as contact node. \textit{dce2.1} also uses one of the nodes of the outer network as a contact node. We made sure that the chosen contact nodes in \textit{dc1} and \textit{dc4} were not connected to each other directly (i.e. with a TCP connection), but rather only through the overlay. In order to send data messages through the whole overlay, we have implemented a simple application that sends \textit{string} objects. Finally, since edge networks are subscribed to keywords, as we previously described, so they retain only some part of the data exchanged, we have subscribed each edge node to the keyword ``test'' only. This was done by using the configuration file in each node. A more complex application using our API was also implemented and will be described and tested for correctness in section \ref{sec:twitter}.

The machines used for the whole \textit{dce2} edge network have four CPU cores as well as 16 gigabytes of RAM. The machine used for the \textit{dce1} was a personal computer with two CPU cores and 3 gigabytes of RAM.

\subsubsection{Join}
An edge node tries to communicate with the specified contact on start. If it fails, it'll try again after 10 seconds. In this tests, we made sure the contact nodes were initiated before the lower level nodes.

We measured the average time it took to exchange data messages with the contact nodes. To be able to obtain a representative value, each edge node connected to a contact node executed an edge broadcast every second. Every edge message is then answered with an ACK, whose time of reception is used to calculate the latency.

The latency of the first request from the node in \textit{dce1} to the contact node in \textit{dc1}, Netherlands, was 476ms in average. Basically, this means establishing a TCP connection to the contact node, sending it an EdgeJoin request and receiving an EdgeJoinResponse. For the contacting node in \textit{dce2}, it took 386ms until receiving the EdgeJoinResponse from the contact node in \textit{dc4}, Canada. On the other hand, it takes 70ms for the node in \textit{dce2.1} to receive the EdgeJoinResponse from \textit{dce2}. This was much faster since both \textit{dce2} and \textit{dce2.1} are in the same physical network.


\subsubsection{Latency and Failures}

Data messages subsequent to the join procedure take, in average: 43ms for the communication between \textit{dce1} and \textit{dc1}, 133ms for the communication between \textit{dce2} and \textit{dc4}, and finally 2ms between \textit{dce2.1} and \textit{dce2}.

Data messages take in average 193ms from \textit{dce1} to \textit{dce2.1} and vice-versa. However, they do not necessarily take the same time (in average) in the same run. In one of the runs, data messages sent from \textit{dce1} took 195ms to reach and be processed by \textit{dce2.1}. On the other hand, messages sent by \textit{dce2.1} took in average 120ms in the same run. This is due to the fact that different trees are used to disseminate the information. When the node in \textit{dce1} sends a message and contacts the contact node in Netherlands, the contact node used the tree rooted at itself to disseminate the information. The same happens for the contact node in Canada when receiving data from \textit{dce2}. Since the trees are different, it results in different dissemination times.

After measuring those values, we registered one active view from the contact node in \textit{dc1} and another one from the contact node in \textit{dc4}. We also made sure those two nodes were not in each other's active views. We then stopped every other node. As the two remaining nodes are not in each other's active view, they will not receive any information about each other via gossip, therefore they will eventually consider them dead. As we previously described, each node attempts to communicate with a random unreachable node every second. Since 38 data center nodes are now dead, it can take a while until they attempt to communicate with each other.

As soon as the edge nodes detect the failures in the contact nodes, which can take up to 20 seconds, they attempt to establish an edge communication with the backup peers (from the contact's active view) previously received. However, until the two data centers' contact nodes consider each other live, no data can be exchanged between them, and in consequence, between \textit{dce1} and \textit{dce2}.

After total recovery, there are two nodes in the data centers and two edge networks. We were able to measure that messages between \textit{dce1} and \textit{dce2} took, in average, 120ms to reach the other edge data center. Since there are only two data center nodes operating, the data center trees are symmetric, which means that latency is the same for both ways.

\subsubsection{Impact of membership changes in edge connections}

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{joinchurn.png}
\caption{Latency in the communication to a contact node} \label{fig:joinchurn}
\end{figure}

We have also measured the impact of a large number of changes in the upper level of the overlay in the communications between the edge networks and the contact nodes.

Firstly, we started only the contact node in \textit{dc4}. After 20 seconds, we started the node in \textit{dce2} that was going to communicate with the contact node. $t = 0$ represents the moment that the edge node was started. At the $t = 50$ we have joined the other 39 data center nodes. Finally, at $t = 240$ we have stopped those. We repeated this test 5 times and calculated the average latency or each $ t $. As we can see, the trend line in the Fig. \ref{fig:joinchurn} is going down with time. The sudden increases in latency can be verified not only while the 39 nodes were up but also in the beginning of the run and some time after they were stopped and after they were detected as failed. Although those higher values can be reached even in stables environments, periodic tree construction and repairing mechanisms may have a slight impact in the latency with a much higher number of nodes. During these runs, we have registered an average latency of approximately the same value when compared to a run with the same number of nodes but without membership changes on-the-fly.



\section{Application example}
\label{sec:twitter}
\subsection{Architecture}
In order to test the functionalities provided by our API as well as our edge service correctness, we implemented a simplified version of Twitter.

Each server node is part of the membership and also handles client connections. Additionally, it is subscribed to a given set of keywords. As we explained before, a node has to be subscribed only to a subset of keywords from the keywords of the nodes in its parent level. The keywords used in this example will be hashtags (e.g. \textit{\#fct}, \textit{\#lisbon}, etc.). A data center node will be subscribed (and therefore interested) to all the used hashtags. An edge network may then be subscribed to the hashtags \textit{\#lisbon, \#unl, \#fct}, and an edge network inside the first one may be subscribed to \textit{\#fct}. The set of keywords to which a node is subscribed is provided beforehand in each node's configuration file. Therefore, it will keep track of every tweet containing those hashtags, maintaining a structure that maps hashtags into lists of tweets.

\subsection{API usage}
Before calling \textit{initServer()} in a server node, a listener has to be added using \textit{addListener()}. The listener class to which we called \textit{GossipTweetListener<Tweet>} implements \textit{BroadcastServiceClient<IPayload>}. It will then implement \textit{receive (Tweet t)}. In our application's case, upon receiving a tweet in which it is interested (\textit{interested(tweet)}), the node will just update its map of hashtags and tweets and then send the tweet \textit{t} to the clients with whom it has an opened connection. The dissemination through the membership is handled elsewhere and shall not be the concern of the application.

Each client communicates with only one server at a time. A client has two possible operations: tweet and search.
\begin{itemize}
    \item \textbf{tweet(Tweet t):} sends \textit{t} to server.
    \item \textbf{search(String hashtag):} asks server for all the tweets containing \textit{hashtag}.
\end{itemize}

Whenever a server receives a tweet from the client (class \textit{Tweet} --- which contains a String, a timestamp and a list of keywords in the string --- implements \textit{IPayload}), it uses the API function \textit{broadcast()}. The tweet will then reach every other server node which is either interested in the payload, in the same overlay as that server, or higher in the hierarchy. Consecutively, if a node received the disseminated tweet and it is interested in it, the tweet will reach any client connected to that same node. Otherwise it will just relay the message.

Whenever a server receives a search request, it will first try to understand whether it is subscribed to \textit{hashtag}. If so, it will just send to the client all the tweets it stored with that hashtag. Otherwise, it will use the result of the API function \textit{where()} to tell the client the address of a server which is subscribed to that keyword. Upon receiving the address, the client's behavior consists solely in closing the current connection, opening connection to the referred server and automatically repeating the operation.

\subsection{Correctness}
We have used our sample application to verify the correctness of our edge service. We have used the configuration files to specify to which keyword is each edge node subscribed. We have used the following configuration:

\begin{itemize}
\item \textit{dce1} - ``\#lisbon, \#portugal''
\item \textit{dce2} - ``\#lisbon, \#unl, \#fct''
\item \textit{dce2.1} - ``\#fct''
\end{itemize}

We then connected one client to the node in \textit{dce1} and another client to the node in \textit{dce2.1} that is directly communicating with \textit{dce2}.

We verified that sent messages that did not include any of the specified keywords were not displayed for the other client. Messages sent with the keyword ``\#lisbon'', on the other end, would only be displayed for the client in \textit{dce1}.

We then used the \textit{search} feature in the \textit{dce2.1} client. As we searched for the keyword ``\#lisbon'', the server node to which the client was connected verified that such keyword was not in its domain. Thereafter, the server answers the client with the IP address of its own contact node, which contains that information. As the client disconnects from the previous server node, contacts the new one and repeats the request, a list of previously sent tweets with the hashtag ``\#lisbon'' are now retrieved to it. After that, any message with keywords in \textit{dce2}'s domain is received and displayed to the client. The same happened to the client in \textit{dc1} when we \textit{searched} for the \textit{hashtag} ``\#europe''. In this case, that client received the IP address of the contact node in \textit{dc1}. The client then closes the open connection and makes the same request to its new server, which retrieves the requested information. Thereafter, since the data center level's domain contains every possible keyword, any subsequent messages, regardless of the keywords, are received by that client.

\subsection{Improvements}
Clients in an edge network are expected to request data contained in the server to which they are connected. As we described, if that is not the case and a client searches for something else (even for only once), a new connection has to be made. This scenario can repeat itself, as we keep searching for other information. As we can see given this implementation, the system will eventually converge to a state where every client is connected to a node in the data center. Although this is a problem that concerns the application itself and not the membership service we described, we still propose three main solutions for this, which can even be used together:
\begin{itemize}
    \item Keeping track of each client's request pattern. As we explained, a client that was once connected to a lower level of the network hierarchy may have requested some information that made him connect to a higher level. If that client goes back to the previous request pattern where he requests information contained in the lower level, we can close the current connection and open another one with a node from that level.
    \item Keeping track of the general request pattern. If a server receives \textit{n} requests containing the keyword \textit{k}, it can subscribe to that keyword and start replicating the corresponding information. However, this implies that the higher levels also subscribe that keyword. This will eventually converge to a scenery where every node is interested in every payload, increasing network traffic and node overload. Nonetheless, this can be mitigated using a maximum number of keywords per node.
    \item Not closing connections. When needing to connect to a higher level node, the client simply keeps the current connection opened. The client can then use one of the opened connections when requesting. Although this allows to keep fast access to the server when possible, we still don't mitigate the convergence problem. 
\end{itemize}

\section{Discussion}
In this chapter we were able to evaluate the most important characteristics of our membership service.

The comparison between MS-DCS and MS-DC1 solutions showed us that, even though we have obtained similar results in most of the tests and only slightly better in others, we can still recover from network partitions as fast and maintain a stable membership with only one connection (per node) to each remote data center.

In the same way, we have showed that our membership can fulfill Cassandra's membership requirements in spite of relying only in partial views. Despite of not being able to show a significant change in the drop of open connections, such mechanism may help in larger-scale scenarios. We took advantage of Cassandra's membership best mechanisms and adapted some others to efficiently maintain a membership with partial views. Moreover, we were able to present significantly low times for nodes joining, which can be very helpful to quickly construct a global view in each node.

We have also verified the correctness of our edge component, which relies on the data center overlay to share information across the nodes and to the edge nodes. Changes in the membership had no impact in the latency of edge message exchanges in our experiments. However, highly larger number of nodes will probably cause an increase of the time per request in edge connections.