\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\babel@toc {english}{}
\babel@toc {portuguese}{}
\babel@toc {english}{}
\babel@toc {portuguese}{}
\babel@toc {english}{}
\babel@toc {english}{}
\babel@toc {english}{}
\babel@toc {english}{}
\babel@toc {english}{}
\babel@toc {portuguese}{}
\babel@toc {english}{}
\babel@toc {portuguese}{}
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {1}Introduction}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Context}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Motivation}{2}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Contributions}{3}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Document organization}{4}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {2}Related Work}{5}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Nodes and peers}{5}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Edge-Computing}{5}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Membership}{6}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Partial views}{7}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Gossip-based protocols}{8}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.1}Strategies}{9}{subsection.2.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Eager push:}{9}{section*.4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Pull:}{9}{section*.5}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Lazy push:}{9}{section*.6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Hybrid:}{9}{section*.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Overlay Network}{9}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.1}Structured Overlays}{10}{subsection.2.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.1.1}Distributed Hash Table (DHT)}{10}{subsubsection.2.5.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.1.2}Chord}{10}{subsubsection.2.5.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.1.3}Pastry}{11}{subsubsection.2.5.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.2}Unstructured Overlays}{11}{subsection.2.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.2.1}SCAMP}{12}{subsubsection.2.5.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.2.2}Cyclon}{12}{subsubsection.2.5.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.2.3}HyParView}{13}{subsubsection.2.5.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.2.4}SWIM}{14}{subsubsection.2.5.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.3}Partial Structured overlays}{14}{subsection.2.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.3.1}T-MAN}{15}{subsubsection.2.5.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.3.2}X-BOT}{15}{subsubsection.2.5.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.3.3}PlumTree}{16}{subsubsection.2.5.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.5.3.4}Thicket}{16}{subsubsection.2.5.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.4}Comparation metrics}{17}{subsection.2.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Connectivity:}{17}{section*.8}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Degree distribution:}{17}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Average Path Length:}{18}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Clustering Coefficient:}{18}{section*.11}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Overlay Cost:}{18}{section*.12}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Accuracy:}{18}{section*.13}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Reliability:}{18}{section*.14}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Redundancy:}{18}{section*.15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Failure detection}{18}{section.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Completeness}{19}{section*.16}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Accuracy}{19}{section*.17}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.1}Models}{19}{subsection.2.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Push}{19}{section*.18}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Pull}{19}{section*.19}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Dual}{19}{section*.20}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.6.1.1}Gupta's failure detector}{20}{subsubsection.2.6.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.2}Adaptive failure detectors}{20}{subsection.2.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6.3}Accrual failure detection}{21}{subsection.2.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Asymptotic completeness:}{21}{section*.21}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Eventual monotony:}{21}{section*.22}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Upper bound:}{21}{section*.23}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Reset:}{21}{section*.24}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.6.3.1}$ \varphi $ accrual failure detector}{21}{subsubsection.2.6.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.7}Cassandra --- architecture and membership}{22}{section.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7.1}Architecture}{23}{subsection.2.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.7.1.1}Partitioning and Replication}{23}{subsubsection.2.7.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.7.2}Membership}{23}{subsection.2.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.8}NAT and firewall-aware Membership}{24}{section.2.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.8.1}Relaying}{24}{subsection.2.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.8.2}Non-relaying}{26}{subsection.2.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.9}Existing solutions}{26}{section.2.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.9.1}Cassandra - Gossip 2.0}{27}{subsection.2.9.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.9.2}Partisan}{28}{subsection.2.9.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.9.3}Serf}{29}{subsection.2.9.3}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Full state sync}{29}{section*.28}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Dedicated gossip layer}{29}{section*.29}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Keeping the state of dead nodes}{29}{section*.30}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Local health}{30}{section*.31}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.10}Summary}{30}{section.2.10}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {3}Membership Service}{31}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Introduction}{31}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}System model}{31}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Architecture}{31}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Simplified view}{31}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Detailed view}{32}{subsection.3.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}Overlay}{34}{subsection.3.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Multi-datacenter and Edge Membership Service}{35}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.1}Peer Sampling Service}{35}{subsection.3.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.1.1}HyParView --- summary}{36}{subsubsection.3.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.1.2}Backup view}{36}{subsubsection.3.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.1.3}Multiple data centers}{37}{subsubsection.3.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.2}Broadcast Tree}{38}{subsection.3.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.2.1}Thicket --- summary}{38}{subsubsection.3.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.2.2}\textit {N} trees}{40}{subsubsection.3.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.2.3}Join mechanism}{40}{subsubsection.3.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.2.4}Local broadcast}{41}{subsubsection.3.4.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.2.5}Dissemination}{42}{subsubsection.3.4.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.2.6}Missing messages}{42}{subsubsection.3.4.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.3}Global view}{42}{subsection.3.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.3.1}Constructing a global view}{43}{subsubsection.3.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.3.2}Closing connections}{44}{subsubsection.3.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.3.3}Shutdown mechanism}{45}{subsubsection.3.4.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.4}Edge Service}{45}{subsection.3.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Bloom filter}{46}{section*.35}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.4.1}Join Mechanism}{46}{subsubsection.3.4.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.4.2}Dissemination}{47}{subsubsection.3.4.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4.5}Failure detection}{48}{subsection.3.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.5.1}On HyParView and Thicket}{48}{subsubsection.3.4.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.5.2}On the Edge Service}{49}{subsubsection.3.4.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.4.5.3}Extensibility}{50}{subsubsection.3.4.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}API}{50}{section.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\textit {IPayload}}{51}{section*.36}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\textit {BroadcastServiceClient<IPayload>}}{51}{section*.37}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.6}Summary}{51}{section.3.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {4}Evaluation}{53}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Experimental work}{53}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Experimental setup}{53}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Multi-data center}{54}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.2.1}Open connections}{54}{subsubsection.4.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.2.2}Messages sent}{56}{subsubsection.4.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.2.3}Join}{57}{subsubsection.4.1.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.2.4}Failure detection and network partitions}{58}{subsubsection.4.1.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{}{59}{section*.44}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Edge Scenario}{59}{subsection.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.3.1}Experimental setup}{59}{subsubsection.4.1.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.3.2}Join}{60}{subsubsection.4.1.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.3.3}Latency and Failures}{61}{subsubsection.4.1.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.3.4}Impact of membership changes in edge connections}{61}{subsubsection.4.1.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Application example}{62}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Architecture}{62}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}API usage}{63}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Correctness}{63}{subsection.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.4}Improvements}{64}{subsection.4.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Discussion}{65}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {5}Conclusion}{67}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Discussion}{67}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Future work}{68}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliography}{69}{section*.48}
